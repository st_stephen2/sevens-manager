﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Globalization;
using System.IO;
using System.Diagnostics;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;

namespace SevensManager
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ObservableCollection<Player> players = new ObservableCollection<Player>();
        SuitLine[] suitLines;
        List<Card> deck = new List<Card>();
        StringBuilder gameResults = new StringBuilder();
        Random random = new Random();

        const string javaPath = @"C:\Program Files\Java\jre7\bin\java.exe";

        public MainWindow()
        {
            InitializeComponent();
            lbPlayers.ItemsSource = players;
            for (int i = 1; i <= 13; i++)
            {
                string namePart = TranslateValue(i);
                deck.Add(new Card(namePart + "C", i, Suit.Clubs));
                deck.Add(new Card(namePart + "D", i, Suit.Diamonds));
                deck.Add(new Card(namePart + "H", i, Suit.Hearts));
                deck.Add(new Card(namePart + "S", i, Suit.Spades));
            }
        }

        private void tbName_TextChanged(object sender, TextChangedEventArgs e)
        {
            btnAddPlayer.Content = "Add " + tbName.Text;
        }

        private void btnAddPlayer_Click(object sender, RoutedEventArgs e)
        {
            if (tbName.Text.Length < 1)
                MessageBox.Show("Please give the player a name.");
            else if (tbExe.Text.Length < 1)
                MessageBox.Show(
                    string.Format("Please specify the path for {0}'s executable.", tbName.Text));
            else
                try
                {
                    players.Add(new Player(tbName.Text, tbExe.Text));
                    tbName.Text = "";
                }
                catch (Exception)
                {
                    MessageBox.Show("Something is wrong with this player. Maybe you made a mistake while typing the executable path.");
                }
        }

        private void btnStart_Click(object sender, RoutedEventArgs e)
        {
            if (cbMultiple.IsChecked != true)
            {
                try
                {
                    gameResults.Clear();
                    MessageBox.Show("The winner is " + ManageGame().ToString());
                    if (cbSaveResults.IsChecked == true)
                    {
                        SaveResults();
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show("The game failed");
                }
            }
            else
            {
                bool multiple;
                string folder = "";
                if (multiple = (cbSaveResults.IsChecked == true && rbMultipleFiles.IsChecked == true))
                {
                    System.Windows.Forms.FolderBrowserDialog saveFolder = new System.Windows.Forms.FolderBrowserDialog();
                    saveFolder.Description = "Where do you want to store the results?";
                    saveFolder.ShowDialog();
                    folder = saveFolder.SelectedPath;
                }

                gameResults.Clear();
                var wins = players.ToDictionary((p) => p.Name, (p) => 0);
                for (int i = 0; i < int.Parse(tbGames.Text); i++)
                {
                    wins[ManageGame().Name]++;
                    if (multiple)
                    {
                        try
                        {
                            using (TextWriter writer = File.CreateText(folder + "\\results" + (i + 1) + ".txt"))
                            {
                                writer.WriteLine(gameResults);
                                gameResults.Clear();
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message, "Uh oh!");
                        }
                    }
                    if (cbRotate.IsChecked == true)
                        players.Move(players.Count() - 1, 0);
                }

                string finalResults = "Wins per player\r\n"
                    + string.Join("\r\n", wins.Select((w) => w.Key + ": " + w.Value.ToString()));

                if (cbSaveResults.IsChecked == true && rbSingleFile.IsChecked == true)
                {
                    try
                    {
                        System.Windows.Forms.SaveFileDialog save = new System.Windows.Forms.SaveFileDialog();
                        save.DefaultExt = ".txt";
                        save.FileName = "results.txt";
                        if (save.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                        {
                            using (TextWriter writer = File.CreateText(save.FileName))
                            {
                                writer.WriteLine(gameResults.ToString());
                                writer.WriteLine(finalResults);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Something isn't right...");
                    }
                }
                else if (multiple)
                {
                    try
                    {
                        using (TextWriter writer = File.CreateText(folder + "\\final_results.txt"))
                        {
                            writer.WriteLine(finalResults);
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Uh oh!");
                    }
                }
                
                MessageBox.Show(finalResults, "The results are in!");
            }
        }

        private void SaveResults()
        {
            System.Windows.Forms.SaveFileDialog save = new System.Windows.Forms.SaveFileDialog();
            save.DefaultExt = ".txt";
            save.FileName = "results.txt";
            if (save.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                try
                {
                    TextWriter writer = File.CreateText(save.FileName);
                    try
                    {
                        writer.Write(gameResults.ToString());
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Something's wrong");
                    }
                    finally
                    {
                        writer.Close();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Something's wrong");
                }
            }
        }

        public static string TranslateValue(int value)
        {
            string translatedValue;
            switch (value)
            {
                case 1:
                    translatedValue = "A";
                    break;
                case 10:
                    translatedValue = "T";
                    break;
                case 11:
                    translatedValue = "J";
                    break;
                case 12:
                    translatedValue = "Q";
                    break;
                case 13:
                    translatedValue = "K";
                    break;
                default:
                    translatedValue = value.ToString();
                    break;
            }
            return translatedValue;
        }

        private Player ManageGame()
        {
            //set up game
            bool gameIsOver = false;
            suitLines = new SuitLine[]{ new SuitLine(Suit.Clubs),
                                   new SuitLine(Suit.Diamonds),
                                   new SuitLine(Suit.Hearts),
                                   new SuitLine(Suit.Spades) };
            for (int i = 0; i < players.Count(); i++)
                players[i].hand.Clear();

            //shuffle cards
            for (int i = 0; i < deck.Count(); i++)
            {
                int r = random.Next(i + 1);
                Card c = deck[r];
                deck[r] = deck[i];
                deck[i] = c;
            }

            //deal cards
            for (int i = 0, c = 0; c < deck.Count(); i++, c++)
            {
                players[i].hand.Add(deck[c]);
                if (i >= players.Count() - 1)
                    i = -1;
            }

            //manage turns
            for (int i = 0; !gameIsOver; i++)
            {
                Player player = players[i];
                var playableCards = player.getPlayableCards(suitLines);

                gameResults.AppendLine("[" + player.Name + "]");
                gameResults.AppendLine(string.Join(" ", player.hand) + " (hand)");

                if (playableCards.Count > 0)
                {
                    try
                    {
                        //call player
                        using (TextWriter writer = File.CreateText(
                            System.IO.Path.Combine(player.Folder, "input.txt")))
                        {
                            writer.WriteLine(string.Join(" ", player.hand));
                            writer.WriteLine(suitLines[0]);
                            writer.WriteLine(suitLines[1]);
                            writer.WriteLine(suitLines[2]);
                            writer.WriteLine(suitLines[3]);
                            writer.Close(); 
                        }

                        gameResults.AppendLine(string.Join(" ", playableCards) + " (playable)");
                        gameResults.AppendLine(suitLines[0].ToString());
                        gameResults.AppendLine(suitLines[1].ToString());
                        gameResults.AppendLine(suitLines[2].ToString());
                        gameResults.AppendLine(suitLines[3].ToString());

                        Process process = new Process();
                        process.StartInfo.UseShellExecute = false;
                        if (player.Executable.EndsWith(".class"))
                        {
                            process.StartInfo.FileName = javaPath;
                            process.StartInfo.Arguments =
                                System.IO.Path.Combine(player.Folder, player.Executable);
                        }
                        else if (player.Executable.EndsWith(".jar"))
                        {
                            process.StartInfo.FileName = javaPath;
                            process.StartInfo.Arguments = "-jar "
                                + System.IO.Path.Combine(player.Folder, player.Executable);
                        }
                        else
                        {
                            process.StartInfo.FileName =
                                System.IO.Path.Combine(player.Folder, player.Executable);
                        }
                        process.StartInfo.WorkingDirectory = player.Folder;
                        process.StartInfo.CreateNoWindow = true;
                        process.Start();
                        process.WaitForExit();

                        //read player's move
                        Card move;
                        TextReader reader = File.OpenText(System.IO.Path.Combine(player.Folder, "move.txt"));
                        try
                        {
                            move = Card.Parse(reader.ReadLine());
                            gameResults.AppendLine().AppendLine(move.ToString() + " (proposed move)");

                            if ((playableCards.Where(
                                (c) => c.Suit == move.Suit && c.Value == move.Value
                                ).Count() < 1))
                                move = playableCards[new Random().Next(playableCards.Count() - 1)];
                        }
                        catch (Exception ex)
                        {
                            //MessageBox.Show(ex.Message);
                            gameResults.AppendLine(string.Format("[Error] {0}", ex.Message));
                            move = playableCards[new Random().Next(playableCards.Count() - 1)];
                        }
                        finally
                        {
                            reader.Close();
                        }

                        //record player's move
                        if (move.Value > 7)
                        {
                            suitLines[(int)move.Suit].High = move.Value;
                        }
                        else if (move.Value < 7)
                        {
                            suitLines[(int)move.Suit].Low = move.Value;
                        }
                        else
                        {
                            suitLines[(int)move.Suit].isEmpty = false;
                            suitLines[(int)move.Suit].High = 7;
                            suitLines[(int)move.Suit].Low = 7;
                        }


                        gameResults.AppendLine(move.ToString() + " (actual move)").AppendLine();

                        //remove card from player's hand
                        player.hand.Remove(player.hand.Single((c) => c.Suit == move.Suit && c.Value == move.Value));

                        //check whether or not the game has ended
                        gameIsOver = players.Where((p) => p.hand.Count() == 0).Count() > 0;
                    }
                    catch (Exception ex)
                    {
                        //MessageBox.Show(ex.Message);
                        gameResults.AppendLine(string.Format("[Error] {0}", ex.Message));
                        gameIsOver = true;
                    }
                }

                if (i >= players.Count() - 1)
                    i = -1;
            }

            //return winner
            return players.Single((p) => p.hand.Count == 0);
        }

        private void tbName_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                btnAddPlayer_Click(btnAddPlayer, e);
                e.Handled = true;
            }
        }

        private void tbGames_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            int result;
            e.Handled = !int.TryParse(e.Text, out result);
        }

        private void lbPlayers_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if(lbPlayers.SelectedIndex>=0)
                btnRemovePlayer.Content = "Remove " + players[lbPlayers.SelectedIndex].Name;
        }

        private void btnRemovePlayer_Click(object sender, RoutedEventArgs e)
        {
            int i = lbPlayers.SelectedIndex;
            players.RemoveAt(i);
        }

        private void btnBrowse_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.OpenFileDialog dialog = new System.Windows.Forms.OpenFileDialog();
            if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                tbExe.Text = dialog.FileName;
        }
    }

    enum Suit
    {
        Clubs, Diamonds, Hearts, Spades
    }

    class SuitLine
    {
        public int High;
        public int Low;
        public Suit Suit;
        public bool isEmpty;

        public SuitLine(Suit suit)
        {
            this.Suit = suit;
            this.isEmpty = true;
        }

        public SuitLine(int high, int low, Suit suit)
        {
            this.High = high;
            this.Low = low;
            this.Suit = suit;
            this.isEmpty = false;
        }

        public override string ToString()
        {
            string suit;
            switch (this.Suit)
            {
                case Suit.Clubs:
                    suit = "C";
                    break;
                case Suit.Diamonds:
                    suit = "D";
                    break;
                case Suit.Hearts:
                    suit = "H";
                    break;
                default:
                    suit = "S";
                    break;
            }
            if (this.isEmpty)
                return suit + "";
            else return suit + (this.High == 7 && this.Low == 7 ? "7" :
                MainWindow.TranslateValue(this.Low).ToString()
                + MainWindow.TranslateValue(this.High).ToString());
        }
    }

    class Card
    {
        string name;

        public int Value;
        public Suit Suit;

        public Card(string name, int value, Suit suit)
        {
            this.name = name;
            this.Value = value;
            this.Suit = suit;
        }

        public static Card Parse(string vschar)
        {
            Suit suit = Card.ParseSuit(vschar[1].ToString());
            int value;
            if (char.IsNumber(vschar, 0))
                value = Int32.Parse(vschar[0].ToString());
            else
                switch (vschar[0])
                {
                    case 'T': value = 10; break;
                    case 'J': value = 11; break;
                    case 'Q': value = 12; break;
                    case 'K': value = 13; break;
                    default: value = 1; break;
                }
            return new Card(vschar, value, suit);
        }

        public bool IsPlayable(SuitLine[] suitLines)
        {
            SuitLine suitLine =
                (from l in suitLines
                 where l.Suit == this.Suit
                 select l).Single<SuitLine>();

            if ((from l in suitLines
                 where l.Suit == Suit.Diamonds
                 select l).Single<SuitLine>().isEmpty)
                return this.Value == 7 && this.Suit == Suit.Diamonds;

            return (!suitLine.isEmpty
                && (this.Value == suitLine.Low - 1
                || this.Value == suitLine.High + 1))
                || this.Value == 7;
        }

        public static Suit ParseSuit(string suit)
        {
            if (suit.StartsWith("C", true, CultureInfo.InvariantCulture))
                return Suit.Clubs;
            else if (suit.StartsWith("H", true, CultureInfo.InvariantCulture))
                return Suit.Hearts;
            else if (suit.StartsWith("S", true, CultureInfo.InvariantCulture))
                return Suit.Spades;
            else
                return Suit.Diamonds;
        }

        public override string ToString()
        {
            return name;
        }
    }

    class Player
    {
        public string Name;
        public string Folder;
        public string Executable;
        public List<Card> hand = new List<Card>();

        public Player(string name, string executable)
        {
            Name = name;

            Regex r = new Regex(@"[^\\]+$");
            Executable = r.Match(executable).Groups[0].Value;
            Folder = executable
                .Remove(executable.Length - Executable.Length);
        }

        public List<Card> getPlayableCards(SuitLine[] suitLines)
        {
            return (from card in hand
                    where card.IsPlayable(suitLines)
                    select card).ToList<Card>();
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
